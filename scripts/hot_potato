#! /usr/bin/env python

from argparse import ArgumentParser
from configparser import ConfigParser
from potato.peel import multi_peeler
from potato import get_data, _DEFAULT_CONFIG

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def getargs():


    help_ = {
        "msname": "Path to/name of input MS. Note the MS is edited! "
                  "Use --backup to create a backup of the original MS.",
        "ras": "RA of source to peel/subtract, in decimal degrees.",
        "decs": "DEC of source to peel/subtract, in decimal degrees.",
        "peel_fovs": "Radius (degrees) about RA, DEC that the TATO will be "
                    "CLEANed/modelled. The actual image size when imaging "
                    "the TATO will be slightly larger to avoid aliasing/gridding "
                    "issues.",
        "image_fov": "A similar radius (in degrees) checking whether the TATO "
                     "would be in the mainlobe image. If the TATO is outside "
                     "of this radius it is peeled/subtracted - "
                     "if not we leave it alone.",
        "names": "Name for the TATO. [Default \"TATO\"].",
        "config": "Configuration file to use for imaging/peeling settings. "
                  "[Default defaults.conf]. ",
        "cleanup": "Switch on to cleanup all intermediate WSClean images. "
                   "CURRENTLY NOT WORKING FULLY.",
        "intermediate": "Switch on to make some additional intermediate WSClean "
                        "images after each peeling/subtraction/etc step. ",
        "intermediate_peels": "Switch on to make some additional images of the "
                              "TATO only.",
        "final": "Switch on to only make the final peeled image.",
        "telescope": "Use a default configuration file for the given telescope. "
                     "The options are [askap, mwa, None]. Default None.",
        "selfcal": "Switch on to perform self-cal interations during peeling. "
                   "Additional `peelsolint` and `peelcalmode` must also be "
                   "supplied.",
        "snr": "Signal-to-noise ratio of model flux density to residual STDDEV "
               "required to attempt peeling. [Default 500].",
        "solint": "Solution interval in seconds for gaincal during peeling. "
                  "Provide multiple to do self-cal. [Default 60].",
        "calmode": "Calibration mode for each gaincal iteration. "
                   "This needs to match the number of solution intervals "
                   "and should be one of [\"p\", \"ap\"]. [Default \"ap\"].",
        "minpeelflux": "Minimum integrated flux density (in Jy) of the TATO "
                       "model required for peeling if not specified, "
                       "use SNR cut. [Default None]",
        "automask": "Auto-masking threshold during self-calibration iterations. "
                    "[Default 10].",
        "minuvpeel": "Minimum uv in lambda for obtaining gain solutions when "
                     "peeling. Not applied to imaging tasks. [Default 0]",
        "minuvimage": "Minimum uv in lambda for imaging tasks. [Default 0].",
        "dump": "Switch on to dump temporary columns to file instead of "
                "holding in memory. May be required on low-memory systems.",
        "backup": "Switch on to create a backup of the input MS. ",
        "bandpass": "Switch on to use CASA's bandpass calibration tool instead "
                    "of gaincal."
    }

    description_ = """
Peel Out Those Annoying, Terrible Objects! Use WSClean to create a model to peel/subtract a bright, off-axis source(s) from data. 
A simplified version of the standard peeler, intended to be used with multiple sources.
"""

    epilog_ = """

"""

    ps = ArgumentParser(description=description_, epilog=epilog_)

    ps.add_argument("msname", type=str, help=help_["msname"])
    ps.add_argument("image_fov", type=float, help=help_["image_fov"])

    ps.add_argument("--ras", nargs="*", type=float, help=help_["ras"])
    ps.add_argument("--decs", nargs="*", type=float, help=help_["decs"])
    ps.add_argument("--peel_fovs", nargs="*", type=float, help=help_["peel_fovs"])
    
    ps.add_argument("-n", "--names", default=None, type=str, nargs="*", help=help_["names"])
    ps.add_argument("-c", "--config", type=str, default=None, help=help_["config"])
    ps.add_argument("-C", "--cleanup", action="store_true", help=help_["cleanup"])
    ps.add_argument("-I", "--intermediate_images", action="store_true",
        dest="intermediate",
        help=help_["intermediate"])
    ps.add_argument("--intermediate_peels", dest="intermediatepeels", 
        action="store_true", help=help_["intermediate_peels"])
    ps.add_argument("-F", "--final", action="store_true")
    ps.add_argument("-t", "--telescope", choices=["askap", "mwa", None], default=None)
    ps.add_argument("-s", "--selfcal", action="store_true")
    ps.add_argument("-S", "--direct_subtract", action="store_true")
    ps.add_argument("-snr", "--peel_snr", "--snr", dest="peelsnr", type=float, default=0.)
    ps.add_argument("-solint", "--peel_solint", dest="peelsolint", nargs="*", default=[60.], type=float)
    ps.add_argument("-calmode", "--peel_calmode", dest="peelcalmode", nargs="*", default=["ap"], type=str)
    ps.add_argument("-automask", "--peel_automask", dest="peelautomask", nargs="*", default=[10.], type=float)
    ps.add_argument("-minpeelflux", "--peel_flux_min", dest="peelminflux", default=0.,
        type=float, help=help_["minpeelflux"])
    ps.add_argument("-peelspectralpol", "--peel_spectral_pol", dest="peelspectralpol", default=None, type=int)
    ps.add_argument("-refant", "--refant", dest="refant", type=int, default=4)
    ps.add_argument("-minuvpeel", "--peel_minuvl", dest="minpeeluvl", default=None, type=float,
        help=help_["minuvpeel"])
    ps.add_argument("-minuvimage", "--image_minuvl", dest="minimageuvl", default=0., type=float,
        help=help_["minuvimage"])
    ps.add_argument("-D", "--dump", action="store_true")
    ps.add_argument("-b", "--backup", action="store_true", help=help_["backup"])
    ps.add_argument("-B", "--bandpass", action="store_true",
        help=help_["bandpass"])
    ps.add_argument("-T", "--tmp", default="./")
    ps.add_argument("-peelimagepad", "--peel_image_pad", dest="peelimagepad", type=float, default=2.)
    ps.add_argument("-imagepad", "--image_pad", dest="imagepad", type=float, default=1.)
    ps.add_argument("-notimename", "--no_time_name", dest="notimename", action="store_true")
    ps.add_argument("-localrms", "--local_rms", dest="localrms", action="store_true")
    ps.add_argument("-nouseshift", "--no-use-shift", dest="useshift", action="store_false")
    ps.add_argument("-wslcean", "--wsclean", default="wsclean")

    args = vars(ps.parse_args())

    if str(args["telescope"]).lower() == "askap" and not args["config"]:
        args["config"] = get_data("askap.conf")
    elif str(args["telescope"]).lower() == "mwa" and not args["config"]:
        args["config"] = get_data("mwa.conf")

    if not args["config"]:
        args["config"] = get_data(_DEFAULT_CONFIG)
    logger.info("Using configuration file {}".format(
        args["config"]
    ))
    
    config = ConfigParser()
    config.read(args["config"])
    defaults = config["default"]
    image = config["image"]
    peel = config["peel"]

    result = dict(defaults)
    result.update({k: v for k, v in dict(image).items() if v is not None})
    result.update({k: v for k, v in dict(peel).items() if v is not None})
    result.update({k: v for k, v in args.items() if v is not None})
    
    if "peelspectralpol" not in result.keys():
        result["peelspectralpol"] = None
    else:
        result["peelspectralpol"] = int(result["peelspectralpol"])

    if "minpeeluvl" not in result.keys():
        result["minpeeluvl"] = 0.

    return result


def main():
    args = getargs()

    multi_peeler(
        msname=args["msname"],
        ras=args["ras"],
        decs=args["decs"],
        peel_fovs=args["peel_fovs"],
        image_fov=args["image_fov"],
        image_pad=args["imagepad"],
        names=args["names"],
        subtract=args["direct_subtract"],
        make_intermediate_images=args["intermediate"],
        make_intermediate_peels=args["intermediatepeels"],
        make_final_image=args["final"],
        make_backup=args["backup"],
        config=args["config"],
        cleanup=args["cleanup"],
        dump_columns=args["dump"],
        peel_solint=args["peelsolint"],
        peel_calmode=args["peelcalmode"],
        peel_sigma=args["peelautomask"],
        peel_snr=args["peelsnr"],
        peel_image_pad=args["peelimagepad"],
        peel_min_flux=float(args["peelminflux"]),
        peel_spectral_pol=args["peelspectralpol"],
        cal_min_uv=args["minpeeluvl"],
        refant=args["refant"],
        image_min_uv=args["minimageuvl"],
        bandpass=args["bandpass"],
        tempdir=args["tmp"],
        no_time_name=args["notimename"],
        local_rms=args["localrms"],
        use_shift=args["useshift"],
        wsclean=args["wsclean"]
    )

if __name__ == "__main__":
    main()