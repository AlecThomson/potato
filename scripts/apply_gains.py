#! /usr/bin/env python

from argparse import ArgumentParser

from potato.msutils import ms_apply_gains

def getargs():

    ps = ArgumentParser(
        description="A small program to *simply* apply *simple* CASA-generated gain tables.",
        epilog="Seriously, this is very *simple* - no checking is made and gains are just plonked right in there for the nearest time interval.")
    ps.add_argument("msname")
    ps.add_argument("gains", help="Name of gains table.")
    ps.add_argument("-c", "--column", default="DATA")
    ps.add_argument("-C", "--output_column", "--output-column", dest="output", default="CORRECTED_DATA")
    ps.add_argument("-u", "--unapply", dest="unapply", action="store_true")
    ps.add_argument("-v", "--verbose", action="store_true")

    args = ps.parse_args()
    return args


def cli(args):
    ms_apply_gains(args.msname, args.gains, args.column, args.output, 
        unapply=args.unapply,
        verbose=args.verbose
    )


if __name__ == "__main__":
    cli(getargs())