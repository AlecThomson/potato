#! /usr/bin/env python

from argparse import ArgumentParser
from potato.cwriter import write_config

def getargs():

    ps = ArgumentParser(description="Make a custom configuration file.")

    ps.add_argument("outname", help="Output configuration file name.")

    ps.add_argument("--image_scale", type=float, default=0.0045,
        help="Image pixel scale in degrees.")
    ps.add_argument("--image_size", type=int, default=8000,
        help="Image size.")
    ps.add_argument("--image_briggs", type=float, default=0.0,
        help="Briggs weighting for image.")
    ps.add_argument("--image_mgain", type=float, default=0.8,
        help="Image major iteration gain factor.")
    ps.add_argument("--image_channels", type=int, default=4,
        help="Number of channels out/subbands for image.")
    ps.add_argument("--image_niter", type=int, default=50000,
        help="Number of minor iterations for image.")
    ps.add_argument("--image_nmiter", type=int, default=3,
        help="Number of major iterations for image.")
    ps.add_argument("--image_multiscale", action="store_true",
        help="Switch on multiscale for image.")
    ps.add_argument("--image_autothreshold", type=float, default=1.0,
        help="Image auto-masking second threshold.")
    ps.add_argument("--image_automask", type=float, default=3.0,
        help="Image auto-masking first threshold.")
    ps.add_argument("--image_nonegative", action="store_true",
        help="No negative CLEAN components for image.")
    ps.add_argument("--image_minuvl", type=float, default=0.0,
        help="Minimum u,v in lambda for image.")


    ps.add_argument("--peel_scale", type=float, default=0.0045,
        help="Peel image pixel scale in degrees.")
    ps.add_argument("--peel_size", type=int, default=1000,
        help="Peel image size.")
    ps.add_argument("--peel_briggs", type=float, default=0.0,
        help="Briggs weighting for peel image.")
    ps.add_argument("--peel_mgain", type=float, default=0.8,
        help="Peel image major iteration gain factor.")
    ps.add_argument("--peel_channels", type=int, default=8,
        help="Number of channels out/subbands for peel image.")
    ps.add_argument("--peel_niter", type=int, default=20000,
        help="Number of minor iterations for peel image.")
    ps.add_argument("--peel_nmiter", type=int, default=7,
        help="Number of major iterations for peel image.")
    ps.add_argument("--peel_multiscale", action="store_true",
        help="Switch on multiscale for peel image.")
    ps.add_argument("--peel_autothreshold", type=float, default=1.0,
        help="Peel image auto-masking second threshold.")
    ps.add_argument("--peel_automask", type=float, default=3.0,
        help="Peel image auto-masking first threshold.")
    ps.add_argument("--peel_nonegative", action="store_true",
        help="No negative CLEAN components for peel image.")
    ps.add_argument("--peel_minuvl", type=float, default=0.0,
        help="Minimum u,v in lambda for peel image.")

    args = ps.parse_args()
    settings = vars(args)
    image_settings = {k.replace("image_", ""): v for k, v in settings.items() if "image" in k}
    peel_settings = {k.replace("peel_", ""): v for k, v in settings.items() if "peel" in k}
    return image_settings, peel_settings, args.outname


def cli():

    image_settings, peel_settings, outname = getargs()
    write_config(image_settings, peel_settings, outname)


if __name__ == "__main__":
    cli()