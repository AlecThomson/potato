#! /usr/bin/env python

from typing import Optional, Tuple, List, Dict

import os
import shutil
from subprocess import Popen, check_call

from astropy.coordinates import SkyCoord
from astropy import units as u

from configparser import ConfigParser
from . import get_data, _DEFAULT_CONFIG

# CASA 6:
try:
    from casatasks import gaincal, bandpass, flagdata, split, delmod
    CASA6 = True
except ImportError:
    CASA6 = False

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


# TODO: make sure alias works so containerised versions can work
# WSCLEAN="singularity run /home/duc030/Dropbox/piip.img wsclean"
# WSCLEAN="wsclean"
WSCLEAN_IMAGES = ["image", "psf", "dirty", "residual", "model"]

if "CASA_LOCATION" in os.environ.keys():
    CASA = os.environ["CASA_LOCATION"]
else:
    CASA = "casa"

class WSCleanWrapper():
    """A wrapper for WSClean. Not all-encompassing.

    Args:
        msname:
            Name of MeasurementSet for imaging.
        data_column:
            Data column for imaging, e.g. DATA, CORRECTED_DATA, MODEL_DATA.
            Can be specified later.
        fits_mask:
            Name of FITS image used as a mask for CLEAN.
            Can be specified later.
        tempdir:
            Temporary directory used by WSClean -temp-dir.

    """

    def __init__(self, msname : str, 
        data_column : str = "", 
        fits_mask : str = "",
        tempdir : str = "./",
        wsclean_location : str = "wsclean"):

        # Read in default configuration file:
        default_cfg = ConfigParser()
        default_cfg.read(get_data(_DEFAULT_CONFIG))
        self.defaults = default_cfg["default"]
        self.runs = 0
        self.images: Dict[int, List[str]] = {}
        self.logfiles: Dict[int, str] = {}
        self.data_column = data_column
        self.fits_mask = fits_mask
        self.msname = msname
        self.lastmodel = ""
        self.lastimage = ""
        self.lastresidual = ""
        self.tempdir = tempdir
        self.wsclean_location = wsclean_location

    def read_config(self, config : str, dataset : str):
        """Read in configuration file to set parameters.

        Options can be overwritten prior to imaging in the usual way, e.g.
        `WSCleanWrapper.scale = 1`.

        Args:
            config:
                Configuration file name.
            dataset:
                Key in configuration file for option set.
            

        """

        user_cfg = ConfigParser()
        user_cfg.read(config)
        self.config = config
        self.dataset = dataset
        
        params = user_cfg[dataset]

        self.scale = params.getfloat(
            "scale", self.defaults.getfloat("scale")
        )
        self.size = params.getint(
            "size", self.defaults.getint("size")
        )
        self.briggs = params.getfloat(
            "briggs", self.defaults.getfloat("briggs")
        )
        self.channels = params.getint(
            "channels", self.defaults.getint("channels")
        )
        self.nmiter = params.getint(
            "nmiter", self.defaults.getint("nmiter")
        )
        self.niter = params.getint(
            "niter", self.defaults.getint("niter")
        )
        self.mgain = params.getfloat(
            "mgain", self.defaults.getfloat("mgain")
        )
        self.auto_mask = params.getfloat(
            "automask", self.defaults.getfloat("automask")
        )
        self.auto_threshold = params.getfloat(
            "autothreshold", self.defaults.getfloat("autothreshold")
        )
        self.nonegative = params.getboolean(
            "nonegative", False
        )
        self.multiscale = params.getboolean(
            "multiscale", False
        )
        self.multiscale_max_scales = params.getint(
            "multiscalemaxscales", 5
        )
        self.multiscale_scale_bias = params.getfloat(
            "multiscalescalebias", 0.7
        )
        self.minuvl = params.getfloat(
            "minimageuvl", self.defaults.getfloat("minimageuvl")
        )
        self.maxuvl = params.getfloat(
            "maximageuvl", self.defaults.getfloat("maximageuvl", None)
        )
        self.spectral_pol = params.getint(
            "peelspectralpol", 2
        )
        self.spectral_pol = params.getint(
            "imagespectralpol", 2
        )
        self.intervals_out = params.getint(
            "intervalsout", self.defaults.getint("intervalsout", None)
        )

    def read_params(self,
        size : int,
        scale : float, 
        briggs : float = 0.0, 
        channels : int = 2,
        nmiter : int = 5,
        niter : int = 10000,
        mgain : float = 0.8,
        auto_mask : float = 3.,
        auto_threshold : float = 1.,
        nonegative : bool = False,
        multiscale : bool = False,
        multiscale_max_scales : int = 5,
        multiscale_scale_bias : float = 0.7,
        minuvl : float = 0.,
        maxuvl: Optional[float] = None,
        intervals_out : Optional[int] = None,
        spectral_pol : int = 2):
        """Store params manually instead of using a config file. Legacy."""

        self.scale = float(scale)
        self.size = int(size)
        self.briggs = float(briggs)
        self.channels = int(channels)
        self.nmiter = int(nmiter)
        self.niter = int(niter)
        self.mgain = float(mgain)
        self.auto_mask = float(auto_mask)
        self.auto_threshold = float(auto_threshold)
        self.nonegative = nonegative
        self.multiscale = multiscale
        self.multiscale_max_scales = int(multiscale_max_scales)
        self.multiscale_scale_bias = float(multiscale_scale_bias)
        self.minuvl = float(minuvl)
        self.maxuvl = maxuvl
        self.spectral_pol = spectral_pol
        self.intervals_out = intervals_out
        

    def wsclean(self, outname, 
        msname : str = "",
        data_column : str = "", 
        fits_mask : str = "",
        nonegative : bool = False,
        no_update_model : bool = False,
        dirty_only : bool = False,
        local_rms : bool = False,
        use_shift: bool = True,
        direction: Optional[Tuple[float,float]] = None,
        ):
        """Run WSClean with preset parameters.

        The most recent image name is stored, and a log file is written.

        Args:
            msname:
                Name of MeasurementSet for imaging. Usually preset.
            data_column:
                Data column for imaging, e.g. DATA, CORRECTED_DATA, MODEL_DATA.
                Usually preset.
            fits_mask:
                Name of FITS image used as a mask for CLEAN. Usually preset.
            nonegative:
                Switch to enable -no-negative. This defaults to a preset value.
            no_update_model:
                Switch to disable updating the modelling column. 
            dirty_only:
                Switch to not do any cleaning (i.e. only make the dirty map).
            use_shift:
                Use shift in WSClean.
            direction:
                Direction for shift in WSClean.

        """

        self.runs += 1
        self.images[self.runs] = []
        
        if not msname:
            msname = self.msname
        if not fits_mask:
            fits_mask = self.fits_mask
        if not data_column:
            data_column = self.data_column
        if not nonegative:
            nonegative = self.nonegative
        if not self.spectral_pol:
            self.spectral_pol = 2

        

        if dirty_only:
            sparams = "-padding 2.0 " + \
                    "-name {} ".format(outname) + \
                    "-niter 1 " + \
                    "-size {0} {0} ".format(self.size) + \
                    "-scale {:.6f} ".format(self.scale) + \
                    "-weight briggs {} ".format(self.briggs) + \
                    "-nmiter 1 ".format(self.nmiter) + \
                    "-minuv-l {} ".format(self.minuvl) + \
                    "-temp-dir {} ".format(self.tempdir)

        else:
            sparams = "-join-channels -no-mf-weighting -use-wgridder -padding 2.0 " + \
                "-name {} ".format(outname) + \
                "-size {0} {0} ".format(self.size) + \
                "-scale {:.6f} ".format(self.scale) + \
                "-weight briggs {} ".format(self.briggs) + \
                "-channels-out {} ".format(self.channels) + \
                "-nmiter {} ".format(self.nmiter) + \
                "-niter {} ".format(self.niter) + \
                "-mgain {} ".format(self.mgain) + \
                "-auto-mask {} ".format(self.auto_mask) + \
                "-auto-threshold {} ".format(self.auto_threshold) + \
                "-minuv-l {} ".format(self.minuvl) + \
                "-fit-spectral-pol {} ".format(self.spectral_pol) + \
                "-temp-dir {} ".format(self.tempdir)

            if self.multiscale:
                sparams += "-multiscale " + \
                    "-multiscale-max-scales {} ".format(self.multiscale_max_scales) + \
                    "-multiscale-scale-bias {} ".format(self.multiscale_scale_bias)
                
            if fits_mask is not None:
                sparams += "-fits-mask {} ".format(fits_mask)
            
            if nonegative:
                sparams += "-no-negative "

            if local_rms:
                sparams += "-local-rms "


        if data_column:
            sparams += "-data-column {} ".format(data_column)

        if no_update_model:
            sparams += "-no-update-model-required "

        if self.maxuvl:
            sparams += "-maxuv-l {} ".format(self.maxuvl)

        if self.intervals_out:
            sparams += "-intervals-out {} ".format(self.intervals_out)

        if use_shift:
            if not direction:
                raise ValueError("Direction required for shift.")
            # change to silly hms:dms convention:
            d = SkyCoord(ra=direction[0]*u.deg, dec=direction[1]*u.deg)
            ra_str = d.ra.to_string(unit=u.hourangle, sep="hms")
            dec_str = d.dec.to_string(sep="dms")

            sparams += f"-shift {ra_str} {dec_str}"

        sparams += " -use-wgridder"
        sparams += " {}".format(msname)

        logfile = outname + ".wsclean.log"

        logger.info("Running WSCLEAN with: {1} {0}".format(sparams, self.wsclean_location))
        with open(logfile, "w+") as log:
            Popen("{0} {1}".format(self.wsclean_location, sparams), 
                shell=True, 
                stdout=log,
                stderr=log).wait()
        
        self.logfiles[self.runs] = logfile 

        if not dirty_only:
            channels = ["{0:0>4}".format(i) for i in range(self.channels)] + ["MFS"]
            for c in channels:
                for image in WSCLEAN_IMAGES:
                    image_name = f"{outname}-{c}-{image}.fits"
                    self.images[self.runs].append(image_name)


            self.lastmodel = "{0}-MFS-model.fits".format(outname)
            self.lastimage = "{0}-MFS-image.fits".format(outname)
            self.lastresidual = "{0}-MFS-residual.fits".format(outname)
        
        else:
            for image in WSCLEAN_IMAGES:
                image_name = f"{outname}-{image}.fits"
                self.images[self.runs].append(image_name)
            # No real model or residuals. 1 iteration is done just get get an
            # approximate bmaj/bmin in the header.
            self.lastimage = "{0}-image.fits".format(outname)
            self.lastmodel = ""
            self.lastresidual = ""


    def cleanup(self, keepimages : List = [], keepchannels : List = [], run : Optional[int] = None):
        """Clean up WSClean output images, keeping some."""
        # TODO: actually have this work - something in the name check isn't working correctly
        # and the wrong files are removed or files are just not removed.
        if not run:
            run = self.runs
        for image in self.images[run]:
            if not bool([i for i in keepchannels if (i in image)]) or \
                not bool([i for i in keepimages if (i in image)]):
                os.remove(image)
                self.images[run].remove(image) 


    def check_log_for_kjy(self, run : Optional[int] = None) -> bool:
        """Check the most recent WSClean log file for CLEAN divergence."""
        if not run:
            run = self.runs
        with open(self.logfiles[self.runs]) as f:
            if "kJy" in f.read():
                return True
            else:
                return False





def gaincal_wrapper(ms : str,
    caltable : str,
    solint : float = 60.,
    calmode : str = "ap",
    solnorm : bool = True,
    minuv : float = 0.,
    data_column : str = "DATA",
    smodel : Optional[List] = None,
    refant : Optional[int] = None,
    minsnr : float = 3) -> str:
    """Wrapper for CASA gaincal.
    
    Args:
        ms:
            MeasurementSet name.
        caltable:
            Name of calibration table.
        solint:
            Solution interval in seconds.
        calmode:
            Calibration mode; one of 'ap', 'a', or 'p'.
        solnorm:
            Switch to normalize amplitudes of solutions.
        minuv:
            Mininum u,v in lambda for calibration.
        data_column:
            Data column to calibrate. NOT USED.

    Returns:
        str:
            Name of calibration table (i.e. `caltable`).
    
    """

    # TODO: gaincal is silly and doesn't allow column selection - 
    #       make temp. data column for gaincal by column dumping?
    # with CASA 6 pip installation this is part of casatasks. Only for Python 3.4/3.6

    if CASA6:
        gaincal(
            vis=ms,
            caltable=caltable,
            solint=solint,
            calmode=calmode,
            solnorm=solnorm,
            uvrange=">{}lambda".format(minuv),
            smodel=smodel,
            minsnr=minsnr
        )
    else:
        # We have to use an external CASA command. 

        # Check again for CASA_LOCATION in environment:
        if "CASA_LOCATION" in os.environ.keys():
            CASA = os.environ["CASA_LOCATION"]
        else:
            CASA = "casa"

        params = "\'{}\', ".format(ms)
        params += "caltable=\'{}\', ".format(caltable)
        params += "solint=\'{}s\', ".format(solint)
        params += "solnorm={}, ".format(solnorm)
        params += "calmode='{}', ".format(calmode)
        params += "uvrange=\'>{}lambda\', ".format(minuv)
        params += "minsnr={}, ".format(minsnr)
        if smodel is not None:
            params += "smodel={}, ".format(smodel)
        if refant is not None:
            params += "refant=\'{}\' ".format(refant)
        gcommand = "gaincal({})".format(params)
        ccommand = "{} --nogui --agg --nologger -c \"{}\"".format(CASA, gcommand)
        logger.debug("Running {}".format(ccommand))
        with open(caltable+".log", "w+") as log:
            check_call(ccommand, shell=True, stdout=log, stderr=log)


    return caltable

def bandpass_wrapper(ms : str,
    caltable : str,
    solint : float = 60.,
    solnorm : bool = False,
    minuv : float = 0.,
    refant : str = "127",
    minblperant : int = 50,
    preavg_ch : Optional[int] = None) -> str:
    """Wrapper for CASA bandpass."""

    if CASA6:
        try:
            bandpass(
                vis=ms,
                caltable=caltable,
                solint=solint,
                solnorm=solnorm,
                uvrange=">{}lambda".format(minuv),
                refant=refant,
                minblperant=minblperant
            )
        except RuntimeError:
            # probably missing antenna 127 - try default of 4:
            logger.warn("Bandpass calibration hit a RunTime error - most likely "
                "the default antenna 127 and/or mininimum baselines per antenna of 50 "
                "(MWA defaults) are not compatible with the observation. "
                "Falling back on normal defaults.")
            bandpass(
                vis=ms,
                caltable=caltable,
                solint=solint,
                solnorm=solnorm,
                uvrange=">{}lambda".format(minuv),
                refant="3",
                minblperant=4,
            )
    else:

        # Check again for CASA_LOCATION in environment:
        if "CASA_LOCATION" in os.environ.keys():
            CASA = os.environ["CASA_LOCATION"]
        else:
            CASA = "casa"

        

        params = "\'{}\', ".format(ms)
        params += "caltable=\'{}\', ".format(caltable)
        if not preavg_ch:
            params += "solint=\'{}s\', ".format(solint)
        else:
            params += "solint=\'{}s,{}ch\', ".format(solint, preavg_ch)
        params += "solnorm={}, ".format(solnorm)
        params += "uvrange=\'>{}lambda\', ".format(minuv)
        if refant is not None:
            params += "refant=\'{}\' ".format(refant)
        gcommand = "bandpass({})".format(params)
        ccommand = "{} --nogui --agg --nologger -c \"{}\"".format(CASA, gcommand)
        logger.debug("Running {}".format(ccommand))
        with open(caltable+".log", "w+") as log:
            check_call(ccommand, shell=True, stdout=log, stderr=log)

    return caltable


def flagdata_wrapper(ms : str,
    flag_mode : str = "rflag",
    data_column : str = "CPARAM"):
    """Wrapper for flagdata, intended for calibration tables."""

    if CASA6:
        flagdata(
            vis=ms,
            mode=flag_mode,
            datacolumn=data_column
        )

    else:

        # Check again for CASA_LOCATION in environment:
        if "CASA_LOCATION" in os.environ.keys():
            CASA = os.environ["CASA_LOCATION"]
        else:
            CASA = "casa"

        params = "\'{}\', ".format(ms)
        params += "mode=\'{}\', ".format(flag_mode)
        params += "datacolumn=\'{}\', ".format(data_column)
        gcommand = "flagdata({})".format(params)
        ccommand = "{} --nogui --agg --nologger -c \"{}\"".format(CASA, gcommand)
        logger.debug("Running {}".format(ccommand))
        with open(ms+".flagdata.log", "w+") as log:
            check_call(ccommand, shell=True, stdout=log, stderr=log)
        

def split_wrapper(ms, outms):
    """Split out DATA column."""

    if CASA6:
        split(vis=ms, outputvis=outms, datacolumn="data")
    else:

        if "CASA_LOCATION" in os.environ.keys():
            CASA = os.environ["CASA_LOCATION"]
        else:
            CASA = "casa"
        
        params = "\'{}\', ".format(ms)
        params += "outputvis=\'{}\'".format(outms)
        gcommand = "split({}, datacolumn=\'data\')".format(params)
        ccommand = "{} --nogui --agg --nologger -c \"{}\"".format(CASA, gcommand)
        logger.debug("Running {}".format(ccommand))
        with open(ms+".split.log", "w+") as log:
            check_call(ccommand, shell=True, stdout=log, stderr=log)

        


def delmod_wrapper(ms):
    """Remove MODEL_DATA column with delmod."""

    if CASA6:
        delmod(vis=ms, scr=True)
    else:

        if "CASA_LOCATION" in os.environ.keys():
            CASA = os.environ["CASA_LOCATION"]
        else:
            CASA = "casa"
        
        params = "\'{}\'".format(ms)
        gcommand = "delmod({}, scr=True)".format(params)
        ccommand = "{} --nogui --agg --nologger -c \"{}\"".format(CASA, gcommand)
        logger.debug("Running {}".format(ccommand))
        with open(ms+".delmod.log", "w+") as log:
            check_call(ccommand, shell=True, stdout=log, stderr=log)


def forcecopytree(file1 : str, file2 : str):
    """A copy and replace equivalent for directories."""

    if os.path.exists(file2):
        shutil.rmtree(file2)
    shutil.copytree(file1, file2)

def forcecopyfile(file1 : str, file2 : str):
    """A copy and replace equivalent for files."""

    if os.path.exists(file2):
        os.remove(file2)
    shutil.copy2(file1, file2)

