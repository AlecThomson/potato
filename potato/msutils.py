#! /usr/bin/env python

"""A collcection of utilities for handling MeasurementSets.

Todo: improve memory performance. Holding full data columns in memory can become
problematic, especially for phase rotation. 

"""

import time
try:
    from memory_profiler import profile
except ImportError:
    pass

from typing import List, Tuple, Optional

import gc

import numpy as np



from casacore.tables import table, maketabdesc, makecoldesc
from astropy.constants import c

import h5py

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)




# numba has some painful requirements and isn't always needed

try:
    from numba import jit, njit, float64, complex128, float32, complex64, prange
    single = float32
    csingle = complex64
    double = float64
    cdouble = complex128
    has_numba = True
except ImportError:
    has_numba = False
    single = np.single
    csingle = np.csingle
    double = np.double
    cdouble = np.cdouble

def use_jit(has_numba):
    def decorator(func):
        if not has_numba:
            logger.warning("No numba - (un-)applying solutions may be slower.")
            return func
        else:
            return jit(func, nopython=True)
    return decorator

c = c.value
"""
We never use the quantities, so it is easiest to assign ``c`` to only its value.
"""


# https://github.com/torrance/radical/blob/master/radical/measurementset.py
# modified to remove filtering
class MeasurementSet(object):
    """Class to open and handle MeasurementSets.
    
    Args:
        filename:
            Filename/path to MeasurementSet.
        datacolumn:
            Name of datacolumn(s) to operate on. If None, DATA will be selected,
            and CORRECTED_DATA and MODEL_DATA data will be added if available.
            
    """
    def __init__(self, filename : str, datacolumn : Optional[List] = None):
        self.mset = table(filename, readonly=False, ack=False)
        mset = self.mset

        self.antids = np.array(range(0, len(mset.ANTENNA)))
        self.ra0, self.dec0 = mset.FIELD.getcell("PHASE_DIR", 0)[0]

        self.freqs = mset.SPECTRAL_WINDOW.getcell("CHAN_FREQ", 0)
        self.midfreq = np.array([(min(self.freqs) + max(self.freqs)) / 2])
        self.lambdas = c / self.freqs
        self.midlambda = c / self.midfreq
        self.ant1 = self.mset.getcol("ANTENNA1")
        self.ant2 = self.mset.getcol("ANTENNA2")
        self.uvw = self.mset.getcol("UVW")
        self._data = None
        self._corrected_data = None
        self._model_data = None
        self.colnames = mset.colnames()
        if not datacolumn:
            datacolumn = ["DATA"]
            if "CORRECTED_DATA" in self.colnames:
                datacolumn.append("CORRECTED_DATA")
            if "MODEL_DATA" in self.colnames:
                datacolumn.append("MODEL_DATA")
        self.datacolumn = datacolumn


    @property
    def data(self):
        if not self._data:
            # self._data = [np.complex128(self.mset.getcol(c)) for c in self.datacolumn] 
            # self._data = [np.csingle(self.mset.getcol(c)) for c in self.datacolumn] 
            self._data = np.cdouble(self.mset.getcol("DATA"))
        return self._data

    @data.setter
    def data(self, data):
        self._data = data

    @data.deleter
    def data(self):
        del self._data


    @property
    def corrected_data(self):
        if not self._corrected_data:
            self._corrected_data = np.cdouble(self.mset.getcol("CORRECTED_DATA"))
        return self._corrected_data

    @corrected_data.setter
    def corrected_data(self, data):
        self._corrected_data = data
    
    @corrected_data.deleter
    def corrected_data(self):
        del self._corrected_data


    @property
    def model_data(self):
        if not self._model_data:
            self._model_data = np.cdouble(self.mset.getcol("MODEL_DATA"))
        return self._model_data
  
    @model_data.setter
    def model_data(self, data):
        self._model_data = data
    
    @model_data.deleter
    def model_data(self):
        del self._model_data


    def __getattr__(self, name):
        return getattr(self.mset, name)


#@profile 
def explicit_free(item):
    del item
    gc.collect()


@use_jit(has_numba)
def apply(data : np.ndarray, 
    ant1 : np.ndarray, 
    ant2 : np.ndarray, 
    time : np.ndarray, 
    g_cparams_xx : np.ndarray, 
    g_cparams_yy : np.ndarray, 
    g_times : np.ndarray, 
    unapply : bool = False,
    verbose : bool = False) -> np.ndarray:
    """Apply/unapply closest-in-time gains to ``data``.

    Todo: linear interpolation between times

    Args:
        data: 
            Data array from e.g. a MeasurementSet. 
        ant1:
            ``ANTENNA1`` in MeasurementSet.
        ant2:
            ``ANTENNA2`` in MeasurementSet. 
        time:
            ``TIME`` in MeasurementSet.
        g_cparams_xx:
            Complex gains for XX.
        g_cparams_yy:
            Complex gains for YY. 
        g_times:
            Times for complex gains.
        unapply:
            Unapply gains. Default operation is to apply gains.
        verbose:
            Increase output. Todo: more output.

    Returns:
        ``data`` array with complex gains (un-)applied.

    """

    def inv(A):
        # faster than numpy.linalg.inv in this case by factor of ~2
        detA = (A[0, 0]*A[1, 1]) - (A[0, 1]*A[1, 0])
        invA = A.copy()
        invA[0, 0] = A[1, 1] / detA
        invA[0, 1] = -A[0, 1] / detA
        invA[1, 0] = -A[1, 0] / detA
        invA[1, 1] = A[0, 0] / detA
        return invA
    
    cshape2 = g_cparams_xx.shape[1]
    dshape2 = data[0].shape[1]


    for n in range(len(data)):

        idx = np.argmin(np.abs(g_times-time[n]))
        # TODO time interp

        for i in range(data[n].shape[0]):
            
            if cshape2 == 1:
                # TODO freq interp
                freq_index = 0
            else:
                freq_index = i

            gains_a = np.zeros((2,2), dtype=np.cdouble)
            gains_a[0, 0] = g_cparams_xx[ant1[n], freq_index, idx]
            gains_a[1, 1] = g_cparams_yy[ant1[n], freq_index, idx]
            
            gains_b = np.zeros((2,2), dtype=np.cdouble)
            gains_b[0, 0] = g_cparams_xx[ant2[n], freq_index, idx]
            gains_b[1, 1] = g_cparams_yy[ant2[n], freq_index, idx]

            # just use reshape? reshape breaks? Confused by new datatype?
            data_i = np.zeros((2,2), dtype=np.cdouble)
            data_i[0, 0] = data[n][i][0]
            data_i[0, 1] = data[n][i][1]
            data_i[1, 0] = data[n][i][2]
            data_i[1, 1] = data[n][i][3]
            # data_i = data[n][i].reshape((2,2))

            if unapply:
                data_i_c = np.dot(np.dot(gains_a, data_i), gains_b.conj().T)
        
            else:
                data_i_c = np.dot(np.dot(inv(gains_a), data_i), inv(gains_b.conj().T))
                
            data[n][i][0] = data_i_c[0, 0]
            data[n][i][3] = data_i_c[1, 1]


    return data


#@profile
def ms_apply_gains(msname: str, gains: str, 
    column : str = "DATA", 
    output_column : str ="CORRECTED_DATA", 
    unapply : bool = False,
    verbose : bool = False,
    ignore_flags : bool = False) -> None:
    """Apply (or un-apply) gains from file to a MeasurementSet.

    Args:
        msname:
            Input MeasurementSet filename/path.
        column:
            Data column in MeasurementSet to apply/un-apply gains. 
        output_column:
            Data column to write gains applied to ``column``.
        unapply:
            Switch to reverse operation, i.e. un-apply gains.
        verbose:
            Switch to increase logging output. 
    
    """

    logger.debug("Un/applying {} from/to {} ({})".format(
        gains, msname, column
    ))
    ms = table(msname, readonly=False, ack=False)

    if column not in ms.colnames():
        ms.close()
        raise RuntimeError("No column in {} of name {}".format(msname, column))

    if output_column != column:
        create_column_like_data(msname, output_column, 
            replace=True, 
            ref_col=column
        )

    
    column = output_column

    g = table(gains, readonly=True, ack=False)
    g_times1 = g.getcol("TIME")
    g_ants1 = g.getcol("ANTENNA1")
    g_cparams1 = g.getcol("CPARAM")
    g_flags = g.getcol("FLAG")
    if ignore_flags:
        g_cparams1[np.where(g_flags == 1)] = complex(1., 0.)
    else:
        g_cparams1[np.where(g_flags == 1)] = np.nan
    g_times = g_times1[np.where(g_ants1 == 0)[0]]
    g_ants = set(g_ants1)
    g_cparams_xx = np.ones((len(g_ants), g_cparams1.shape[1], len(g_times)), dtype=np.cdouble)
    g_cparams_yy = np.ones((len(g_ants), g_cparams1.shape[1], len(g_times)), dtype=np.cdouble)

    for antenna in g_ants:

        # print(g_cparams1[np.where(g_ants1 == antenna)[0]][:, :, 0].T.shape)
        g_cparams_xx[antenna, ...] = g_cparams1[np.where(g_ants1 == antenna)[0]][:, :, 0].T
        g_cparams_yy[antenna, ...] = g_cparams1[np.where(g_ants1 == antenna)[0]][:, :, 1].T

    
    # this may be memory intensive for large datasets...
    # TODO: add more considerate apporach with striping.
    data = ms.getcol(column)
    ant1 = ms.getcol("ANTENNA1")
    ant2 = ms.getcol("ANTENNA2")
    time = ms.getcol("TIME")

    data = apply(data, ant1, ant2, time, g_cparams_xx, g_cparams_yy, g_times, 
        unapply=unapply,
        verbose=verbose)

    ms.putcol(column, data)

    # explicit_free([data, ant1, ant2, time])
    # del data
    ms.flush()
    ms.close()
    g.close()


# https://github.com/torrance/radical/blob/master/radical/phaserotate.py
#@profile
def phase_rotate(uvw, data, ra, dec, ra0, dec0, lambdas):
    """
    
    """
    
    # Calculate rotated uvw values
    start = time.time()
    new_uvw = rotateuvw(uvw, ra, dec, ra0, dec0).astype(np.single)
    elapsed = time.time() - start
    logger.debug("Phase rotated uvw elapsed: {}".format(elapsed))


    # Calculate phase offset
    start = time.time()
    new_data = [woffset(data_i, uvw.T[2], new_uvw.T[2], lambdas) for data_i in data]
    elapsed = time.time() - start
    logger.debug("Phase rotated visibilities elapsed: {}".format(elapsed))
    
    return new_uvw.astype(np.double), new_data

#@profile
def phase_rotate_uvw(uvw, ra, dec, ra0, dec0):

    start = time.time()
    new_uvw = rotateuvw(uvw, ra, dec, ra0, dec0)
    elapsed = time.time() - start
    logger.debug("Phase rotated uvw elapsed: {}".format(elapsed))
    return new_uvw


#@profile
def phase_rotate_column(ms, col, uvw, new_uvw, lambdas):

    start = time.time()
    new_data = woffset(ms.mset.getcol(col), uvw.T[2], new_uvw.T[2], lambdas)
    elapsed = time.time() - start
    logger.debug("Phase rotated visibilities elapsed: {}".format(elapsed))
    # del data
    ms.mset.putcol(col, new_data)
    return new_data


# https://github.com/torrance/radical/blob/master/radical/phaserotate.py
#@profile
def rotateuvw(uvw, ra, dec, ra0, dec0):
    """
    We calculate new uvw values based on existing uvw values. Whilst this has the effect
    of propagating any existing uvw errors, it has the benefit of being mathematically
    self-consistent.

    Adopted from matrix equation 4.1, in Thompson, Moran, Swenson (3rd edition).
    Let (uvw) = r(ra, dec) * (xyz), then this formula is: r(ra, dec) * r^-1(ra0, dec0)
    """
    u, v, w = uvw.T
    uvwprime = np.empty_like(uvw)

    uvwprime[:, 0] = (
        u * np.cos(ra - ra0)
        + v * np.sin(dec0) * np.sin(ra - ra0)
        - w * np.cos(dec0) * np.sin(ra - ra0)
    )
    uvwprime[:, 1] = (
        -u * np.sin(dec) * np.sin(ra - ra0)
        + v * (np.sin(dec0) * np.sin(dec) * np.cos(ra - ra0) + np.cos(dec0) * np.cos(dec))
        + w * (np.sin(dec0) * np.cos(dec) - np.cos(dec0) * np.sin(dec) * np.cos(ra - ra0))
    )
    uvwprime[:, 2] = (
        u * np.cos(dec) * np.sin(ra - ra0)
        + v * (np.cos(dec0) * np.sin(dec) - np.sin(dec0) * np.cos(dec) * np.cos(ra - ra0))
        + w * (np.sin(dec0) * np.sin(dec) + np.cos(dec0) * np.cos(dec) * np.cos(ra - ra0))
    )
    return uvwprime


# https://github.com/torrance/radical/blob/master/radical/phaserotate.py

# @njit([complex128[:, :, :](complex128[:, :, :], float64[:], float64[:], float64[:])], parallel=True)
# @njit([csingle[:, :, :](csingle[:, :, :], single[:], single[:], single[:])], parallel=True)
#@profile
def woffset(data, oldw, neww, lambdas):
    # logger.debug("making offsets")
    offset = -2j * np.pi * (neww - oldw)
    # logger.debug("making phase")
    # phase = np.empty_like(data)

    # return data
    # tt1 = data.copy()
    # tt2 = data.copy()
    # tt3 = data.copy()

    # for row in prange(0, data.shape[0]):
    for row in range(0, data.shape[0]):
        tmp = offset[row] / lambdas
        for pol in range(0, data.shape[2]):
            # phase[row, :, pol] = np.exp(tmp)
            data[row, :, pol] *= np.exp(tmp)

    # return data
    # return np.exp(phase)
    # return data*phase
    return data


# @profile
def do_rotate(msname, ra, dec, datacolumn):
    """Calculate rotations and applied to datacolumn and uvw coordinates."""

    datacolumn = list(set(datacolumn))

    ms = MeasurementSet(msname, datacolumn=datacolumn)
    ra = np.radians(ra)
    dec = np.radians(dec)

    # TODO: loop over columns instead of holding all in memory.

    logger.debug("Rotating columns: {}".format(ms.datacolumn))

    uvw = phase_rotate_uvw(
        uvw=ms.uvw, 
        ra=ra, 
        dec=dec, 
        ra0=ms.ra0,
        dec0=ms.dec0
    )

    for column in ms.datacolumn:
        
        phase_rotate_column(
            ms=ms,
            col=column,
            uvw=ms.uvw, 
            new_uvw=uvw,
            lambdas=ms.lambdas
        )
        
    ms.mset.putcol("UVW", uvw)
    ms.mset.flush()
    ms.mset.close()

    field = table(msname+"/FIELD", readonly=False, ack=False)
    field.putcell('PHASE_DIR', 0, np.array([[ra, dec]]))
    field.flush()
    field.close()



def get_phase_direction(msname):
    """Get current phase direction from msname."""
    field = table(msname+"/FIELD", ack=False)
    pd = field.getcol("PHASE_DIR")
    coords = pd[0][0]
    return np.degrees(coords[0]), np.degrees(coords[1])

def add_columns(ms1, col1, col2, ms2=None):
    """Add col1 from ms1 to col2 in ms2.
    
    ms1 can be the same as ms2 and defaults to ms2.
    """
    
    if not ms2:
        ms2 = ms1
    t1 = table(ms1, readonly=False, ack=False)
    t2 = table(ms2, readonly=True, ack=False)
    c1 = t1.getcol(col1)
    c2 = t2.getcol(col2)
    c1 += c2
    t1.putcol(col1, c1)
    t1.close()
    t2.close()

def subtract_columns(ms1, col1, col2, ms2=None):
    """Subtract col1 from ms1 from col2 in ms2.
    
    ms1 can be the same as ms2 and defaults to ms2.
    """
    if not ms2:
        ms2 = ms1
    t1 = table(ms1, readonly=False, ack=False)
    t2 = table(ms2, readonly=True, ack=False)
    c1 = t1.getcol(col1)
    c2 = t2.getcol(col2)
    c1 -= c2
    t1.putcol(col1, c1)
    t1.close()
    t2.close()

def replace_columns(ms1, col1, col2, ms2=None):
    """Replace col1 from ms1 with col2 from ms2."""
    if not ms2:
        ms2 = ms1
    t1 = table(ms1, readonly=False, ack=False)
    t2 = table(ms2, readonly=True, ack=False)
    c1 = t1.getcol(col1)
    c2 = t2.getcol(col2)
    c1 = c2
    t1.putcol(col1, c1)
    t1.close()
    t2.close()

def dump_column(ms, col, name):
    """Dump col from ms into numpy pickle file."""
    t = table(ms, readonly=False, ack=False)
    logger.debug("dumping")
    c = t.getcol(col)
    h5f = h5py.File(name, "w")
    logger.debug("creating h5 dataset")
    h5f.create_dataset(col, data=c)
    del c
    h5f.close()
    
    # c.dump(name)
    
    t.close()

def undump_column(ms, col, name):
    """Undump numpy pickle file into col from ms."""
    t = table(ms, readonly=False, ack=False)
    # dumped = np.load(name, allow_pickle=True)
    logger.debug("undumping")
    h5f = h5py.File(name, "r")
    logger.debug("accessing col")
    c = h5f[col][:]
    h5f.close()
    t.putcol(col, c)
    t.flush()
    t.close()

def getcol(ms, col):
    """Get col from ms."""
    return table(ms, readonly=True, ack=False).getcol(col)

def putcol(ms, col, data):
    """Put col with data into ms."""
    t = table(ms, readonly=False, ack=True)
    t.putcol(col, data)
    t.close()


#@profile
def create_column_like_data(ms, col, replace=False, ref_col="DATA"):
    
    t = table(ms, readonly=False, ack=False)
    if replace and col in t.colnames():
        t.removecols(col)
        t.flush()
    elif col in t.colnames():
        raise RuntimeError("{} already in {} and not replacing".format(col, ms))
    cdesc = t.getcoldesc(ref_col)
    dminfo = t.getdminfo(ref_col)
    dminfo["NAME"] = col
    cdesc["comment"] = "The {} column".format(col)
    t.addcols(maketabdesc(makecoldesc(col, cdesc)), dminfo)
    data_ = t.getcol(ref_col)
    if col == "MODEL_DATA":
        data_[:] = 0.
    t.putcol(col, data_)
    t.flush()
    t.close()
    

def get_nchan(ms):
    t = table(ms+"/SPECTRAL_WINDOW", readonly=True, ack=False)
    nchan = t.getcol("NUM_CHAN")[0]
    t.close()
    return nchan


def rmcol(ms, col):
    """Remove column (e.g. MODEL_DATA) from MS."""
    with table(ms, readonly=False) as ms:
        ms.removecols(col)
    