#! /usr/bin/env python

import os
from datetime import datetime
from typing import List, Any, Optional, Tuple

from astropy.coordinates import SkyCoord
from astropy import units as u
import numpy as np

from .msutils import (get_phase_direction,
                            dump_column,
                            undump_column,
                            do_rotate, 
                            ms_apply_gains, 
                            subtract_columns,
                            add_columns, 
                            putcol,
                            getcol,
                            get_nchan)
from .fitsutils import (make_template_image,
                              make_fits_mask,
                              get_source_flux,
                              get_std,
                              get_sum)
from .wrappers import (forcecopytree,
                             gaincal_wrapper,
                             bandpass_wrapper,
                             flagdata_wrapper,
                             WSCleanWrapper)

import logging
logging.basicConfig(format="%(levelname)s (%(module)s): %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

WARN_PIX = 10000

def time():
    return "{}".format(datetime.now()).split()[1][0:8]


def peel(Imager : WSCleanWrapper, 
    outbase : str,
    solint : list = [60.],
    calmode : list = ["ap"],
    sigma : list = [10.],
    cal_minuv : float = 0.,
    cleanup : bool = True,
    dump_columns : bool = True,
    subtract : bool = True,
    keep_mfs_image : bool = True,
    bandpass : bool = False,
    tempdata  = None,
    refant = None,
    intermediate_peels = False,
    reuse_initial = False,
    no_time_name : bool = False,
    local_rms : bool = False,
    use_shift: bool = True,
    direction: Optional[Tuple[float,float]] = None,
    ):
    
    if len(solint) != len(calmode):
        raise RuntimeError(
            "Each self-cal loop (`solint`) requires a calibration mode (`calmode`) - " + 
            "these lists should be equal length.")
    if len(sigma) < len(solint):
        sigma = [sigma[0]]*len(solint)  # just use the same sigma

    gains = []
    if no_time_name:
        timenow = "0"
    else:
        timenow = time()

    if len(solint) > 1 and not tempdata:
        if dump_columns:
            data = outbase + ".data.{}".format(timenow)
            dump_column(Imager.msname, Imager.data_column, data)
        else:
            data = getcol(Imager.msname, Imager.data_column)
    elif tempdata is not None:
        data = tempdata

    # if bandpass:
    #     preavg_ch =int(get_nchan(Imager.msname) / Imager.channels)
    # else:
    # TODO: for now channels in bandpass solutions have to equal output channels
    preavg_ch = None

    original_sigma = Imager.auto_mask
    # original_minuv = Imager.minuvl
    for i in range(len(solint)):
        logger.debug("Selfcal round {} with {}, {}, {}".format(
            i, solint[i], calmode[i], sigma[i]
        ))
        # do number of iterations equal to number of separate solints specified:
        # first get initial model:
        Imager.auto_mask = sigma[0]
        # Imager.nonegative = True
        # Imager.nonegative = False
        if reuse_initial and i == 0:
            logger.debug("Using initial model")
        else:
            Imager.wsclean(
                outbase+".{}.{}.forpeel".format(timenow, i), 
                local_rms=local_rms,
                use_shift=use_shift,
                direction=direction
            )

        if bandpass and (len(solint) == 1 or i > 0):
            logger.debug("Bandpass solutions with minuv {} lambda".format(cal_minuv))
            g = bandpass_wrapper(
                ms=Imager.msname,
                caltable=outbase+".{}cal.{}".format(i, timenow),
                solint=solint[i],
                minuv=cal_minuv,
                refant=refant,
                preavg_ch=preavg_ch,
                solnorm=False
                
            )
        else:
            logger.debug("Gains solutions with minuv {} lambda".format(cal_minuv))
            g = gaincal_wrapper(
                ms=Imager.msname,
                caltable=outbase+".{}cal.{}".format(i, timenow),
                solint=solint[i],
                calmode=calmode[i],
                minuv=cal_minuv,
                data_column=Imager.data_column,
                refant=refant,
                solnorm=False
            )
            
    
        if not os.path.exists(g):
            logger.warning("{} does not exist - calibration likely failed.".format(
                g
            ))
            return None, Imager

        logger.info("Flagging calibration table: {}".format(
            g
        ))
        flagdata_wrapper(
            ms=g,
            flag_mode="rflag",
            data_column="CRARAM"
        )

        gains.append(g)
            

        if len(solint) > 1:
            ms_apply_gains(
                msname=Imager.msname,
                gains=g,
                column=Imager.data_column,
                output_column=Imager.data_column,
                ignore_flags=True
            )

            if intermediate_peels:
                Imager.wsclean(
                    outbase+".{}.{}.afterselfcal".format(timenow, i), 
                    no_update_model=True,
                    use_shift=use_shift,
                    direction=direction
                )
    
    if len(solint) > 1 or tempdata is not None:
        if dump_columns:
            undump_column(Imager.msname, Imager.data_column, data)
        else:
            putcol(Imager.msname, Imager.data_column, data)

    if len(solint) > 1:
        Imager.auto_mask = original_sigma
        Imager.wsclean(
            outbase+".{}.{}.forpeel".format(timenow, i+1),
            use_shift=use_shift,
            direction=direction,
        )

    for g in gains[::-1]:
        logger.debug("Unapplying {}".format(g))
        ms_apply_gains(
            msname=Imager.msname,
            gains=g,
            column="MODEL_DATA",
            output_column="MODEL_DATA",
            unapply=True,
            ignore_flags=True
        )
    
    if subtract:
        subtract_columns(
            ms1=Imager.msname,
            col1=Imager.data_column,
            col2="MODEL_DATA"
        )

    # Imager.nonegative = False

    return gains, Imager


def peeler(msname : str,
    ra : float,
    dec : float,
    peel_fov : float,
    image_fov : float,
    image_pad : float = 1.,
    peel_region_file : Optional[str]  = None,
    data_column : str = "DATA",
    peel_snr : float = 500.,
    subtract_snr : float = 1.,
    subtract : bool = True,
    peel_solint : List[float] = [60.], 
    peel_calmode : List[str] = ["ap"],
    peel_sigma : List[float] = [10.],
    peel_min_flux : Optional[float] = None,
    peel_image_pad : float = 2.,
    peel_spectral_pol : int = 2,
    image_min_uv : float = 0.,
    image_max_uv: Optional[float] = None,
    cal_min_uv : float = 0.,
    refant : Optional[int] = None,
    name : str = "TATO",
    make_intermediate_images : bool = False,
    make_intermediate_peels : bool = False,
    make_final_image : bool = False,
    make_backup : bool = True,
    config : Optional[str] = None,
    cleanup: bool = False,
    subtract_mainlobe : bool = False,
    mainlobe_min_flux : Optional[float] = None,
    mainlobe_snr : float = 250.,
    mainlobe_peel_factor : float = 0.5,
    dump_columns : bool = True,
    bandpass : bool = False,
    tempdir : str = "./",
    no_time_name : bool = False,
    local_rms : bool = False,
    use_shift: bool = True,
    wsclean : str = "wsclean",
    ) -> None:
    """Peeling pipeline.

    This runs the pipeline to check whether peeling or subtraction is required,
    and then performs the relevant actions based on the combination of thresholds
    provided. 
    
    TATO: That Annoying Terrible Object.

    Parameters
    -----------
    msname : str
        Name/filepath of MeasurementSet.
    ra, dec : float
        RA and declination of of TATO in decimal degrees.
    peel_fov : float
        FoV as a radius in degrees for peeling/subtracting around TATO. 
    image_fov : float
        FoV as a radius in degrees for mainlobe image. TATO is not peeled/subtracted
        if it is within this radius.
    image_pad:
    peel_region_file: str
        DS9 region file to use as a CLEAN mask during peeling/subtracting.
        If not selected, an aperture based on `ra`, `dec`, and `peel_fov` 
        is made instead.
    data_column : str
        Name of data column. Default "DATA".
    subtract_snr : float
        Minimum SNR required for subtraction. Default 1.
    subtract : bool
        Switch to enable direct subtraction after peeling (or if peeling is not done). Default is to do subtraction.
    peel_solint : list, float
        Solution intervals for self-calibration loops for peeling in seconds. Default [60].
    peel_calmode : list, str
        Calibration type - "a", "p", "ap" for amplitude and phase calibration. Default ["ap"].
    peel_sigma : list, float
        Sigma threshold for CLEAN for self-calibration loops during peeling. Default [10].
    peel_min_flux : float
        Minimum model flux density required for peeling. Default None.
    peel_snr : float
        Minimum model SNR required for peeling. Default 500.
    peel_min_uv : float
        Mininum UV distance in lambda used for imaging + calibration. Default 0.
    refant : int
        Reference antenna. Default None.
    name : str
        Name of TATO. Default "TATO".
    make_intermediate_images : bool
        Make and retain intermediate images of the mainlobe during process. Default False.
    make_intermediate_peels : bool
        Make and retain intermediate images of the TATO during the process. Default False.
    make_backup : bool
        Make backup of MS. Default False.
    config : str
        Configuration file to get options from. Default None.
    cleanup : bool
        Switch to clean up intermediate images (e.g. PSF, dirty maps, residuals). Default False.
    subtract_mainlobe : bool
    dump_columns : bool, default True
        If doing mainlobe subtraction, switch True to dump out intermediate
        columns to disk as numpy pickles to read back later. If switched to False,
        then columns are kept in memory and applied as needed. Note for large datasets
        memory requirements will be large, but also write speeds may be slow, so your
        mileage may vary.
    use_shift : bool
        Use shift to rotate data to TATO direction. Default True.
        If False, use `chgcentre` (in do_rotate function) to rotate data instead.


    Returns
    -------
    None

    """

    if peel_fov > image_fov:
        logger.warning("Peeling FOV is larger than image FOV - check these are the right way around!")

    # First make a backup of the original MS:
    if make_backup:
        forcecopytree(msname, msname+".bkup")
    outbase = msname.replace(".ms", "")

    # TODO: log file for various images?
    if no_time_name:
        timenow = "0"
    else:
        timenow = time()
    
    # Get some directions, including as a SkyCoord object for 
    # easy calculations later. Might not be necessary to import SkyCoord
    # just for a .separation calculation, though
    image_direction = get_phase_direction(msname)
    tato_direction = (ra, dec)
    

    ImageDirection = SkyCoord(ra=image_direction[0],
        dec=image_direction[1],
        unit=(u.deg, u.deg)
    )
    TatoDirection = SkyCoord(ra=tato_direction[0],
        dec=tato_direction[1],
        unit=(u.deg, u.deg),
    )

    Image = WSCleanWrapper(
        msname=msname, 
        data_column=data_column, 
        tempdir=tempdir,
        wsclean_location=wsclean
    )
    
    Tato = WSCleanWrapper(
        msname=msname, 
        data_column=data_column, 
        tempdir=tempdir,
        wsclean_location=wsclean
    )

    if config is not None:
        # TODO: set even if no config passed to command line
        Image.read_config(config, "image")
        Tato.read_config(config, "peel")
    Tato.spectral_pol = peel_spectral_pol
    Tato.minuvl = cal_min_uv

    Image.size = int(np.ceil(2*image_pad * image_fov / Image.scale))  # no padding required
    # TODO: user-set factor for image size
    Tato.size = int(np.ceil(2*peel_image_pad * peel_fov / Tato.scale))  # padding for aliasing reasons
    if Image.size%2 == 1:
        Image.size += 1
    if Tato.size%2 == 1:
        Tato.size += 1
    logger.debug("Setting output IMAGE size: {}".format(Image.size))
    logger.debug("Setting output TATO ({}) size: {}".format(name, Tato.size))

    if Image.size > WARN_PIX:
        logger.warning("IMAGE size > {} pixels per side.".format(WARN_PIX))
    if Tato.size > WARN_PIX:
        logger.warning("TATO ({}) size > {} pixels per side".format(name, WARN_PIX))

    # make a template image and fits mask for mainlobe image:
    image_mask = make_fits_mask(
        template=make_template_image(
            crval=image_direction,
            npix=Image.size,
            cdelt=Image.scale
        ),
        coords=image_direction,
        radius=image_fov,
        outname=outbase+"-mask.fits"
    )

    # make a similar mask for the TATO:
    tato_mask = make_fits_mask(
        template=make_template_image(
            crval=tato_direction,
            npix=Tato.size,
            cdelt=Tato.scale
        ),
        coords=tato_direction,
        radius=peel_fov,
        region_file=peel_region_file,
        outname=outbase+"-{}-mask.fits".format(name)
    )


    Tato.fits_mask = tato_mask
    # Image.fits_mask = image_mask

    if subtract_mainlobe:
        logger.debug("Using mask {} for main image.".format(image_mask))
        fmask = image_mask
        Image.fits_mask = image_mask
        
    else:
        logger.debug("Not using main image mask.")
        fmask = ""
        Image.fits_mask = ""

    if make_intermediate_images:
        
        Image.wsclean(
            outbase+".{}.initial".format(timenow), 
            data_column=data_column,
            fits_mask=fmask,
            no_update_model=True,
            local_rms=local_rms
        )
        if cleanup: Image.cleanup(["image"], ["MFS"])

    # check if the TATO is within the image mask:
    sep = TatoDirection.separation(ImageDirection).value
    logger.debug("{} is {} degrees away".format(name, sep))    

    tempdata = None
    mainlobe_dumped = False

    if sep < image_fov:
        logger.info("{} inside image FOV - not doing anything.".format(name))
        return

    peeling, iter = True, 0  # this is awful, what I am even doing here
    while iter < 2:

        logger.info("{} outside of image FOV - eliminating.".format(name))
        logger.debug("Beginning on iteration {}".format(iter))



        if not use_shift:
            if subtract_mainlobe and iter == 0:  # only dump original data in first iteration
                if dump_columns:
                    original_data = msname + ".data"
                    dump_column(msname, data_column, original_data)
                else:
                    original_data = getcol(msname, data_column)
            
                do_rotate(msname, tato_direction[0], tato_direction[1],
                    datacolumn=["DATA", data_column]
                
                )

            if subtract_mainlobe and iter == 0:  # only dump original data in first iteration
                # rotate to direction of TATO:
                
                if dump_columns:
                    tato_data = msname + ".tato"
                    dump_column(msname, data_column, original_data)
                else:
                    tato_data = getcol(msname, data_column)

            if not peel_min_flux:
                peel_min_flux = 0.
            if not mainlobe_min_flux:
                mainlobe_min_flux = 0.5*peel_min_flux


        if iter == 0 and (peel_min_flux > 0 or peel_snr > 0):  # no need to do this agin for mainlobe subtraction!
            # do not use negative components for calibration + initial image:
            # if make_intermediate_images or make_intermediate_peels:

            # Tato.nonegative = True  # get full model flux
            if no_time_name:
                timenow = "0"
            else:
                timenow = time()
            original_minuv = Tato.minuvl
            Tato.minuvl = image_min_uv  # for initial image for checking flux
            Tato.maxuvl = image_max_uv
            # TODO: do not clean for initial image - use dirty image only
            Tato.wsclean(
                outbase+".{}.{}.{}.initial".format(name, iter, timenow),
                data_column=data_column,
                fits_mask=tato_mask,
                nonegative=True,  # get full model flux
                dirty_only=True,
                no_update_model=True,
                local_rms=local_rms,
                use_shift=use_shift,
                direction=tato_direction,
            )
            
            Tato.minuvl = original_minuv
            if Tato.check_log_for_kjy():
                raise RuntimeError("CLEAN diverged - try turning off multiscale.")
            
            # check SNR for peeling
            std, flux_int, flux_peak = get_source_flux(
                Tato.lastimage, tato_direction, peel_fov, sigma=3.
            )
            logger.debug("stddev: {}, flux ({}, {})".format(
                std, flux_int, flux_peak
            ))
            
            snr = flux_int/std
            

            logger.debug("{} model SNR: {}, model flux: ({} {})".format(name, snr, flux_int, flux_peak))

        elif iter == 0:
            # Force peeling to skip initial image making
            snr, flux_int, flux_peak = 1e30, 1e30, 1e30        
            
        
        # TODO: this logic is awkward - better way to do it?
        if ((flux_int >= peel_min_flux) and (snr >= peel_snr) and not subtract_mainlobe):
            # (iter == 1 and (model_sum >= mainlobe_min_flux and snr >= mainlobe_snr)):
        
            logger.debug("peel, iter={}".format(iter))
            
            gains, Tato = peel(
                Imager=Tato,
                outbase=outbase+".{}.{}".format(name, iter),
                solint=peel_solint,
                calmode=peel_calmode,
                cal_minuv=cal_min_uv,
                sigma=peel_sigma,
                dump_columns=dump_columns,
                subtract=True,
                bandpass=bandpass,
                tempdata=tempdata,
                refant=refant,
                intermediate_peels=make_intermediate_peels,
                no_time_name=no_time_name,
                local_rms=local_rms,
                use_shift=use_shift,
                direction=tato_direction,
            )

            if not gains:
                logger.warning("Peeling not done - check gaincal/bandpass logs.")
            

            if make_intermediate_peels or make_intermediate_images:
                if no_time_name:
                    timenow = "0"
                else:
                    timenow = time()
                Tato.wsclean(
                    outbase+".{}.{}.{}.afterpeel".format(name, iter, timenow),
                    data_column=data_column,
                    fits_mask=tato_mask,
                    dirty_only=True,  # everything should be gone, right?
                    no_update_model=True,
                    use_shift=use_shift,
                    direction=tato_direction,
                )
            

            if make_intermediate_images:
                if not use_shift:
                    do_rotate(msname, image_direction[0], image_direction[1],
                        datacolumn=["DATA", data_column]
                    )

                if no_time_name:
                    timenow = "0"
                else:
                    timenow = time()
                Image.wsclean(
                    outbase+".{}.{}.afterpeel".format(iter, timenow),
                    data_column=data_column,
                    fits_mask=fmask,
                    no_update_model=True,
                    local_rms=local_rms,
                )
                if cleanup: Image.cleanup(["image"], ["MFS"])

                if not use_shift:
                    do_rotate(msname, tato_direction[0], tato_direction[1],
                        datacolumn=["DATA", data_column]
                    )
                
            peeling = True



        else:
            peeling = False
            logger.debug("not peeling, iter={}".format(iter))
            


            # subtract_mainlobe = False  # don't both if not peeling

        if snr > subtract_snr and subtract:
            # Subtract - if already peeled, this subtracts some residual emission due
            # to a different in amplitudes when using calmode=ap and solnorm, etc

            # This may not need to be done again if peeled - the residuals are small
            # and unlikely make a difference in the final image.

            # Also, if peeling is not done due to SNR limits, then direct subtraction
            # may be sufficient. 


            logger.debug("subtraction, iter={}".format(iter))


            if no_time_name:
                timenow = "0"
            else:
                timenow = time() 
            
            Tato.wsclean(
                outbase+".{}.{}.{}.forsubtract".format(name, iter, timenow),
                data_column=data_column,
                fits_mask=tato_mask,
                nonegative=False,
                local_rms=local_rms,
                use_shift=use_shift,
                direction=tato_direction,
            )
            if cleanup: Tato.cleanup(["image"], ["MFS"])
            if Tato.check_log_for_kjy():
                raise RuntimeError("CLEAN diverged - try turning off multiscale.")

            if not peeling and mainlobe_dumped:
                
                logger.debug("Returning mainlobe before subtracting {} model".format(
                    name
                ))
                if not use_shift:
                    do_rotate(msname, image_direction[0], image_direction[1],
                        datacolumn=["DATA", data_column]
                    )

                if dump_columns:
                    undump_column(msname, data_column, original_data)
                else:
                    putcol(msname, data_column, original_data)

                if not use_shift:
                    do_rotate(msname, tato_direction[0], tato_direction[1],
                        datacolumn=["DATA", data_column]
                    )

            logger.debug("Subtracting column for iter={}".format(iter))
            subtract_columns(
                ms1=msname,
                col1=data_column,
                col2="MODEL_DATA",

            )

            if make_intermediate_images or make_intermediate_peels:
                if no_time_name:
                    timenow = "0"
                else:
                    timenow = time()

                Tato.wsclean(
                    outbase+".{}.{}.{}.aftersubtract".format(name, iter, timenow),
                    data_column=data_column,
                    fits_mask=tato_mask,
                    no_update_model=True,
                    local_rms=local_rms,
                    use_shift=use_shift,
                    direction=tato_direction,
                    )

            if not use_shift:
                do_rotate(msname, image_direction[0], image_direction[1],
                    datacolumn=["DATA", data_column]
                )

            if make_intermediate_images or make_final_image or subtract_mainlobe:

                if no_time_name:
                    timenow = "0"
                else:
                    timenow = time()
            
                Image.wsclean(
                    outbase+".{}.{}.aftersubtract".format(iter, timenow), 
                    data_column=data_column,
                    fits_mask=fmask,
                    no_update_model=False,
                    local_rms=True  # restricted CLEANing to avoid removing artefacts of peeled source if it's in the FOV
                )
                if cleanup: Image.cleanup(["image"], ["MFS"])
                
                if subtract_mainlobe and \
                    (flux_int >= mainlobe_min_flux) and \
                    (snr >= mainlobe_snr):

                    # subtract out mainlobe:
                    logger.debug("Subtracting main lobe")
                    logger.debug("Undumping original data for iteration {}".format(iter))
                    if dump_columns:
                        undump_column(msname, data_column, original_data)
                    else:
                        putcol(msname, data_column, original_data)
                    mainlobe_dumped = True

                    tempdata = tato_data

                    subtract_columns(
                        ms1=msname,
                        col1=data_column,
                        col2="MODEL_DATA"
                    )

                    # repeat above check for SNR/flux with (perhaps) lower values:
                    peel_min_flux *= mainlobe_peel_factor
                    peel_snr *= mainlobe_peel_factor
                    # peel_min_flux = mainlobe_min_flux
                    # peel_snr = mainlobe_snr
            
                
        elif not subtract:
            logger.info("Not doing direct subtraction")
            if not use_shift:
                logger.info("- rotating back in place")
                do_rotate(msname, image_direction[0], image_direction[1],
                    datacolumn=["DATA", data_column]
                )
            mainlobe_dumped = False

        else:
            logger.info("{} too faint for peeling or subtraction.".format(name))
            if not use_shift:
                # Do nothing but rotate back to image
                do_rotate(msname, image_direction[0], image_direction[1],
                    datacolumn=["DATA", data_column]
                )

            subtract_mainlobe = False
            mainlobe_dumped = False

        if not subtract_mainlobe:
            iter = 2
        else:
            iter += 1
            subtract_mainlobe = False




def multi_peeler(msname : str,
    ras : float,
    decs : float,
    peel_fovs : float,
    image_fov : float,
    image_pad : float = 1.,
    data_column : str = "DATA",
    peel_snr : float = 500.,
    subtract_snr : float = 1.,
    subtract : bool = True,
    peel_solint : List[float] = [60.], 
    peel_calmode : List[str] = ["ap"],
    peel_sigma : List[float] = [10.],
    peel_min_flux : Optional[float] = None,
    peel_image_pad : float = 2.,
    peel_spectral_pol : int = 2,
    image_min_uv : float = 0.,
    image_max_uv: Optional[float] = None,
    cal_min_uv : float = 0.,
    refant : Optional[int] = None,
    names : Optional[str] = None,
    make_intermediate_images : bool = False,
    make_intermediate_peels : bool = False,
    make_final_image : bool = False,
    make_backup : bool = True,
    config : Optional[str] = None,
    cleanup: bool = False,
    dump_columns : bool = True,
    bandpass : bool = False,
    tempdir : str = "./",
    no_time_name : bool = False,
    local_rms : bool = False,
    use_shift : bool = True,
    wsclean : str = "wsclean",
    ) -> None:

    # First make a backup of the original MS:
    if make_backup:
        forcecopytree(msname, msname+".bkup")
    outbase = msname.replace(".ms", "")

    if no_time_name:
        timenow = "0"
    else:
        timenow = time()

    image_direction = get_phase_direction(msname)

    ImageDirection = SkyCoord(ra=image_direction[0],
        dec=image_direction[1],
        unit=(u.deg, u.deg)
    )
    
    Image = WSCleanWrapper(
        msname=msname, 
        data_column=data_column, 
        tempdir=tempdir,
        wsclean_location=wsclean
    
    )
    if config is not None:
        Image.read_config(config, "image")
    Image.size = int(np.ceil(2*image_pad * image_fov / Image.scale))  # no padding required
    if Image.size%2 == 1:
        Image.size += 1
    logger.debug("Setting output IMAGE size: {}".format(Image.size))
    if Image.size > WARN_PIX:
        logger.warning("IMAGE size > {} pixels per side.".format(WARN_PIX))

    fmask = ""
    Image.fits_mask = ""

    if make_intermediate_images:
        
        Image.wsclean(
            outbase+".{}.initial".format(timenow), 
            data_column=data_column,
            fits_mask=fmask,
            no_update_model=True,
            local_rms=local_rms
        )
        if cleanup: Image.cleanup(["image"], ["MFS"])

    if names is None:
        names = ["TATO_{}".format(i) for i in range(len(ras))]

    # Check initial apparent brightess, then order by total apparent flux:
    tato_directions, tatos = [], []
    app_flux = []

    for ra, dec, peel_fov, name in zip(ras, decs, peel_fovs, names):

        tato_direction = (ra, dec)
        
        TatoDirection = SkyCoord(ra=tato_direction[0],
            dec=tato_direction[1],
            unit=(u.deg, u.deg),
        )

        Tato = WSCleanWrapper(
            msname=msname, 
            data_column=data_column, 
            tempdir=tempdir,
            wsclean_location=wsclean
        )

        if config is not None:
            Tato.read_config(config, "peel")
        Tato.spectral_pol = peel_spectral_pol
        Tato.minuvl = cal_min_uv
        Tato.size = int(np.ceil(2*peel_image_pad * peel_fov / Tato.scale))
        if Tato.size%2 == 1:
            Tato.size += 1
        logger.debug("Setting output TATO ({}) size: {}".format(name, Tato.size))
        if Tato.size > WARN_PIX:
            logger.warning("TATO ({}) size > {} pixels per side".format(name, WARN_PIX))

        tato_mask = make_fits_mask(
            template=make_template_image(
                crval=tato_direction,
                npix=Tato.size,
                cdelt=Tato.scale
            ),
            coords=tato_direction,
            radius=peel_fov,
            outname=outbase+"-{}-mask.fits".format(name)
        )
        Tato.fits_mask = tato_mask

        sep = TatoDirection.separation(ImageDirection).value
        logger.debug("{} is {} degrees away".format(name, sep))

        if sep < image_fov:
            logger.info("{} inside image FOV - skipping.".format(name))
            continue
        if not use_shift:
            do_rotate(msname, TatoDirection.ra.value, TatoDirection.dec.value,
                datacolumn=["DATA", data_column]
            
            )
        original_minuv = Tato.minuvl
        Tato.minuvl = image_min_uv  # for initial image for checking flux
        Tato.maxuvl = image_max_uv
        Tato.wsclean(
            outbase+".{}.{}.initial".format(name, timenow),
            data_column=data_column,
            fits_mask=Tato.fits_mask,
            nonegative=True,  # get full model flux
            dirty_only=True,
            no_update_model=True,
            local_rms=local_rms,
            use_shift=use_shift,
            direction=tato_direction,
        )          

        Tato.minuvl = original_minuv
        if Tato.check_log_for_kjy():
            raise RuntimeError("CLEAN diverged - try turning off multiscale.")
        # check SNR for peeling
        std, flux_int, flux_peak = get_source_flux(
            Tato.lastimage, tato_direction, peel_fov, sigma=3.
        )
        logger.debug("{} stddev: {}, flux ({}, {})".format(
            name, std, flux_int, flux_peak
        ))
        
        snr = flux_int/std
        logger.debug("{} model SNR: {}, model flux: ({}, {})".format(name, snr, flux_int, flux_peak))

        Tato.snr = snr
        Tato.flux_int = flux_int
        Tato.flux_peak = flux_peak
        Tato.name = name

        if not np.isnan([flux_int, flux_peak, snr]).any():
            app_flux.append(flux_int)
            tatos.append(Tato)
            tato_directions.append(TatoDirection)

    # Work out order of peeling - brightest first
    peel_order = np.argsort(app_flux)[::-1]
    tempdata = None

    logger.info("Peeling (in order): {}".format(
        " ".join([tatos[i].name for i in peel_order])
    ))

    if not peel_min_flux:
        peel_min_flux = 0.

    logger.debug("Minimum peel peak flux {}".format(peel_min_flux))

    for p in peel_order:

        Tato = tatos[p]
        TatoDirection = tato_directions[p]
        tato_direction = (TatoDirection.ra.value, TatoDirection.dec.value)
        
        logger.info("{} outside of image FOV - eliminating.".format(Tato.name))
        if not use_shift:
            do_rotate(msname, TatoDirection.ra.value, TatoDirection.dec.value,
                datacolumn=["DATA", data_column]
            )

        if ((Tato.flux_peak >= peel_min_flux) and (Tato.snr >= peel_snr)):

            gains, Tato = peel(
                Imager=Tato,
                outbase=outbase+".{}".format(Tato.name),
                solint=peel_solint,
                calmode=peel_calmode,
                cal_minuv=cal_min_uv,
                sigma=peel_sigma,
                dump_columns=dump_columns,
                subtract=True,
                bandpass=bandpass,
                tempdata=tempdata,
                refant=refant,
                intermediate_peels=make_intermediate_peels,
                no_time_name=no_time_name,
                local_rms=local_rms,
                use_shift=use_shift,
                direction=tato_direction,
            )

            if not gains:
                logger.warning("Peeling not done - check gaincal/bandpass logs.")

            if make_intermediate_peels or make_intermediate_images:
                if no_time_name:
                    timenow = "0"
                else:
                    timenow = time()
                Tato.wsclean(
                    outbase+".{}.{}.afterpeel".format(Tato.name, timenow),
                    data_column=data_column,
                    fits_mask=tato_mask,
                    dirty_only=True,  # everything should be gone, right?
                    no_update_model=True,
                    use_shift=use_shift,
                    direction=tato_direction,
                )
            

            if make_intermediate_images:
                
                if not use_shift:
                    do_rotate(msname, image_direction[0], image_direction[1],
                        datacolumn=["DATA", data_column]
                    )

                if no_time_name:
                    timenow = "0"
                else:
                    timenow = time()
                Image.wsclean(
                    outbase+".{}.afterpeel".format(timenow),
                    data_column=data_column,
                    fits_mask=fmask,
                    no_update_model=True,
                    local_rms=local_rms
                )
                if cleanup: Image.cleanup(["image"], ["MFS"])

                if not use_shift:
                    do_rotate(msname, TatoDirection.ra.value, TatoDirection.dec.value,
                        datacolumn=["DATA", data_column]
                    )
             
        else:
            logger.debug("not peeling")


        if snr > subtract_snr and subtract:
            if no_time_name:
                timenow = "0"
            else:
                timenow = time() 

            Tato.wsclean(
                outbase+".{}.{}.forsubtract".format(Tato.name, timenow),
                data_column=data_column,
                fits_mask=Tato.fits_mask,
                nonegative=False,
                local_rms=local_rms,
                use_shift=use_shift,
                direction=tato_direction,
            )
            if cleanup: Tato.cleanup(["image"], ["MFS"])
            if Tato.check_log_for_kjy():
                raise RuntimeError("CLEAN diverged - try turning off multiscale.") 


            logger.debug("Subtracting column")
            subtract_columns(
                ms1=msname,
                col1=data_column,
                col2="MODEL_DATA",

            )

            if make_intermediate_images or make_intermediate_peels:
                if no_time_name:
                    timenow = "0"
                else:
                    timenow = time()

                Tato.wsclean(
                    outbase+".{}.{}.aftersubtract".format(Tato.name, timenow),
                    data_column=data_column,
                    fits_mask=Tato.fits_mask,
                    no_update_model=True,
                    local_rms=local_rms,
                    use_shift=use_shift,
                    direction=tato_direction,
                    )

            if not use_shift:
                do_rotate(msname, image_direction[0], image_direction[1],
                    datacolumn=["DATA", data_column]
                )

            if make_intermediate_images or make_final_image:

                if no_time_name:
                    timenow = "0"
                else:
                    timenow = time()
            
                Image.wsclean(
                    outbase+".{}.aftersubtract".format(timenow), 
                    data_column=data_column,
                    fits_mask=fmask,
                    no_update_model=False,
                    local_rms=True  # restricted CLEANing to avoid removing artefacts of peeled source if it's in the FOV
                )
                if cleanup: Image.cleanup(["image"], ["MFS"])
        
        elif not subtract:
            logger.info("Not doing direct subtraction.")
        else:
            logger.info("{} too faint for peeling or subtraction.".format(name))

    if not use_shift:
        do_rotate(msname, image_direction[0], image_direction[1],
            datacolumn=["DATA", data_column]
        )





        













