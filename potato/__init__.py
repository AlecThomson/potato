import os

_ROOT = os.path.abspath(os.path.dirname(__file__))
_DEFAULT_CONFIG = "defaults.conf"

def get_data(path):
    return os.path.join(_ROOT, "conf", path)