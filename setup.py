#! /usr/bin/env python

import setuptools

with open("README.md", "r") as f:
    long_description = f.read()

reqs = [
    "astropy",
    "numpy",
    "scipy",
    "python-casacore",
    "h5py",
    "regions",
]

scripts = [
    "scripts/potato",
    "scripts/hot_potato",
    "scripts/peel_configuration.py",
    "scripts/phase_rotate.py",
    "scripts/apply_gains.py",
    "scripts/make_fits_mask.py",
    "scripts/get_pos.py",
    "scripts/get_data_column_only.py"
]

data = {
    "potato": ["conf/*.conf"]
}

setuptools.setup(
    name="potato",
    version="3.0.0",
    author="SWD",
    author_email="stefanduchesne@gmail.com",
    description="Peel out that annoying (and terrible) object!",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/Sunmish/potato",
    install_requires=reqs,
    packages=["potato"],
    package_data=data,
    scripts=scripts
)
