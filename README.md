# PotatoPeel and `potato`
**P**eel **o**ut **t**hat **a**nnoying, **t**errible **o**bject. 

A pipeline to enabling simple peeling and subtraction of bright, off-axis sources for ASKAP's [^1] [RACS](https://research.csiro.au/racs/home/survey/) [^2] project. The pipeline makes use of `CASA` [^3] and `wsclean` [^4] for calibration and imaging parts of the process. 

Not suitable for larger datasets due to current ballooning memory requirements as MeasurementSets require at three times their disk size in RAM currently.

## Documentation
Current *rough* documentation is located at [https://potatopeel.readthedocs.io/en/latest/](https://potatopeel.readthedocs.io/en/latest/).

***Currently documentation is a work in progress!***

Project avatar by Pippa Keel.


[^1]: Australia SKA Pathfinder, Hotan et al. ([2021](https://ui.adsabs.harvard.edu/abs/2021PASA...38....9H/abstract)).
[^2]: Rapid ASKAP Continuum Survey,  McConnell et al. ([2020](https://ui.adsabs.harvard.edu/abs/2020PASA...37...48M/abstract)). 
[^3]: Common Astronomy Software Applications, currently using `python` module versions but will add an option external command line calls to `gaincal` to avoid the current `python==3.6` or `python==3.4` requirements.
[^4]: [WSClean](https://gitlab.com/aroffringa/wsclean):, Offringa et al. ([2014](https://ui.adsabs.harvard.edu/abs/2014MNRAS.444..606O/abstract), [2017](https://ui.adsabs.harvard.edu/abs/2017MNRAS.471..301O/abstract)).
